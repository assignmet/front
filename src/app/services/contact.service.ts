import {Injectable} from '@angular/core';
import {Http, Response,Headers} from '@angular/http';
import {Observable} from 'rxjs/Rx';


@Injectable()
export class ContactService {

  private headers = new Headers();
  private path: String;
  constructor(public authNotHttp: Http) {
    this.path = document.location.protocol +'//'+ document.location.hostname + ':4040';
    this.headers.append('Accept', 'application/json');
    this.headers.append('Content-Type', 'application/json');
      //this.headers.append('x-access-token', key);
  }
  handleError(error) {
    return Observable.throw(error.json().error || 'Server error');
  }
  saveContact(data) {
    return this.authNotHttp.post(this.path + '/contacts', JSON.stringify(data), {headers:this.headers})
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }
  list(){
    return this.authNotHttp.get(this.path + '/contacts', {headers:this.headers})
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }
}
