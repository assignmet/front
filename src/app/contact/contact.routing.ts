import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContactComponent } from './contact.component';

const routes: Routes = [
    { path: '', component: ContactComponent  },
    { path: '**', redirectTo: '' , pathMatch: 'full' }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
