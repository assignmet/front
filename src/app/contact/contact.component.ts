import { Component, OnInit, Input, ViewChild, AfterViewChecked } from '@angular/core';
import { ContactService } from '../services/contact.service';
import { ActivatedRoute,Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';

import {  ResponseMessage, Contact } from './contact.interface';

/*
 * We're loading this component asynchronously
 * We are using some magic with es6-promise-loader that will wrap the module with a Promise
 * see https://github.com/gdi2290/es6-promise-loader for more info
 */

 @Component({
    selector: 'contact',
    providers: [ContactService],
    styleUrls: ['./contact.component.scss'],
    templateUrl: './contact.component.html'
 })

export class ContactComponent implements OnInit {
  @Input() public fName: string;
  @Input() public lName: string;
  @Input() public contactEmail: string;
  @Input() public contactTelephone: string;
  @Input() public contactSubject: string;
  @Input() public contactMessage: string;
  public contactForm: FormGroup;
  public responseMessage: ResponseMessage;
  public showAlert: boolean;

  public validationMessages = {
    fName: {
      required:      'First name is required',
      // minlength:     'Name must be at least 3 characters long',
      maxlength:     'First name cannot be more than 50 characters long'
    },
    lName: {
      required:      'Last name is required',
      // minlength:     'Name must be at least 3 characters long',
      maxlength:     'Last name cannot be more than 50 characters long'
    },
    contactEmail: {
      required:      'Email is required',
      // minlength:     'Email must be at least 8 characters long',
      maxlength:     'Email cannot be more than 50 characters long',
      pattern:       'Enter valid email'
    },
    contactTelephone: {
      required:      'Telephone number is required',
      // minlength:     'Telephone number must be at least 10 characters long',
      maxlength:     'Telephone number cannot be more than 16 characters long',
      pattern:       'Enter valid phone number +99 (999) 999-9999'
    }
  };

  public formErrors = {
    fName: '',
    lName: '',
    contactEmail: '',
    contactTelephone: '',
  };

  public data: Object;

  constructor(
   public contactService: ContactService,
    private fb: FormBuilder,
    private titleService: Title,
    private router:Router
  ) {
    this.titleService.setTitle('Contact');

    this.responseMessage = {
      message: '',
      validMessage: false
    };
    this.showAlert = false;
  }

  public ngOnInit() {
    this.buildForm();
  }

  public buildForm(): void {
    this.contactForm = this.fb.group({
      fName: [this.fName, [
        Validators.required,
        // Validators.minLength(3),
        Validators.maxLength(50)
          // forbiddenNameValidator(/bob/i)
        ]
      ],
      lName: [this.lName, [
        Validators.required,
        // Validators.minLength(3),
        Validators.maxLength(50)
          // forbiddenNameValidator(/bob/i)
        ]
      ],
      contactEmail: [this.contactEmail, [
        Validators.required,
        // Validators.minLength(5),
        Validators.maxLength(50),
        Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i)
        ]
      ],
      contactTelephone: [this.contactTelephone, [
        Validators.required,
        // Validators.minLength(10),
        Validators.maxLength(18),
        Validators.pattern(/^\+\d{2}[ ]?[(]\d{3}[)][ ]?\d{3}[-]\d{4}$/i)
        ]
      ],

    });
    this.contactForm.valueChanges
      .subscribe((data) => this.onValueChanged(data));
    this.onValueChanged(); // (re)set validation messages now
  }

  public onSubmit() {
    const data: Contact = {
      fName: this.contactForm.controls.fName.value,
      lName: this.contactForm.controls.lName.value,
      email: this.contactForm.controls.contactEmail.value,
      phonenumber: this.contactForm.controls.contactTelephone.value,
    };
    this.responseMessage = this.saveContact(data);
    this.showAlert = true;
    this.contactForm.reset();
  }



  private saveContact(value: Contact): ResponseMessage {
    let responseMessage: ResponseMessage = {
      message: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span><p>Sending your enquiry ...</p></span>',
      validMessage: true
    };
    this.contactService.saveContact(value).subscribe(
    (data) => {
        this.router.navigate(['/']);
      },
    (err) => {
        responseMessage.message = 'Please try again shortly';
        responseMessage.validMessage = false;
      }
    );
     return responseMessage;
  }

  private onValueChanged(data?: any) {
    if (!this.contactForm) { return; }
    const form = this.contactForm;

    for (const field in this.formErrors) {
      if (true) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          if (true) {
            for (const key in control.errors) {
              if (true) {
              this.formErrors[field] += messages[key] + ' ';
              }
            }
          }
        }
      }
    }
  }


}
