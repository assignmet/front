import { Routes } from '@angular/router';
import { ContactComponent } from './contact/contact.component';
import { ContactListComponent } from './contact-list/contact-list.component';



import { DataResolver } from './app.resolver';

export const ROUTES: Routes = [

  { path: '', component: ContactListComponent },

  { path: 'add-contact', component: ContactComponent },

  { path: '**',    component: ContactComponent },
];
