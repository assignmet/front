import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContactListComponent } from './contact-list.component';

const routes: Routes = [
    { path: '', component: ContactListComponent  },
    { path: '**', redirectTo: '' , pathMatch: 'full' }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
