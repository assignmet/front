import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { ContactListComponent } from './contact-list.component';
import { routing } from './contact.routing';

@NgModule({
    imports: [
        routing,
        CommonModule,
        ReactiveFormsModule,
    ],
    declarations: [ContactListComponent],
    entryComponents: [ContactListComponent],
    exports: [
        ContactListComponent
    ]
})

export class ContactModule {}
