export interface Contact {
  fName: string;
  lName: string;
  email: string;
  phonenumber: string;
}

export interface ResponseMessage {
  message: string;
  validMessage: boolean;
}
