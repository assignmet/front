import { Component, OnInit } from '@angular/core';
import { ContactService } from '../services/contact.service';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';


/*
 * We're loading this component asynchronously
 * We are using some magic with es6-promise-loader that will wrap the module with a Promise
 * see https://github.com/gdi2290/es6-promise-loader for more info
 */

 @Component({
    selector: 'contact-list',
    providers: [ContactService],
    styleUrls: ['./contact-list.component.scss'],
    templateUrl: './contact-list.component.html'
 })

export class ContactListComponent implements OnInit {
public listdata;

  constructor(
   public contactService: ContactService,
    private titleService: Title,
  ) {
    this.titleService.setTitle('Contact-list');

  }

  public ngOnInit() {
    this.contactService.list().subscribe(
    (data) =>{this.listdata = data;},

    (err) => {

      }
    );

  }


    // this.contactService.saveContact(value).subscribe(
    // (data) => {
    //     responseMessage.message = 'We will contact you shortly';
    //     responseMessage.validMessage = true;
    //   },
    // (err) => {
    //     responseMessage.message = 'Please try again shortly';
    //     responseMessage.validMessage = false;
    //   }
    // );



}
